import 'package:connectivity/connectivity.dart';
import 'package:flutter_movie/utils.dart' as utils;

abstract class Api {

  Future<ConnectivityResult> checkConnectivity({throwIfNot = true}) async {
    var connectivityResult = await Connectivity().checkConnectivity();
    print(connectivityResult);
    if (throwIfNot && connectivityResult == ConnectivityResult.none) {
      throw Exception(utils.Label.INTERNET_NOT_AVAILABLE);
    }
    return connectivityResult;
  }
}