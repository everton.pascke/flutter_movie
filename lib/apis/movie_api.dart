import 'package:flutter_movie/apis/api.dart';
import 'package:flutter_movie/environment.dart' as env;
import 'package:flutter_movie/models/model_utils.dart' as utils;
import 'package:flutter_movie/models/movie.dart';
import 'package:flutter_movie/models/pagination.dart';
import 'package:http/http.dart' as http;

class MovieApi extends Api {
  static final MovieApi _singleton = MovieApi._internal();

  factory MovieApi() {
    return _singleton;
  }

  MovieApi._internal();

  Future<Movie> findById(int id) async {
    checkConnectivity(throwIfNot: true);
    var url = '${env.Api.baseUrl}/movie/$id?api_key=${env.Api.key}';
    print('get ~> $url');
    http.Response response = await http.get(url);
    return utils.Decode.movie(response);
  }

  Future<Pagination> findAllPopulars(int page) async {
    checkConnectivity(throwIfNot: true);
    var url = '${env.Api.baseUrl}/movie/popular?api_key=${env.Api.key}&language=${env.Api.language}&page=$page';
    print('get ~> $url');
    http.Response response = await http.get(url);
    return utils.Decode.pagination(response);
  }
}
