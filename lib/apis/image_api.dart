import 'package:flutter_movie/apis/api.dart';
import 'package:flutter_movie/environment.dart' as env;
import 'package:flutter_movie/models/images.dart';
import 'package:flutter_movie/models/model_utils.dart' as utils;
import 'package:http/http.dart' as http;

class ImageApi extends Api {
  static final ImageApi _singleton = ImageApi._internal();

  factory ImageApi() {
    return _singleton;
  }

  ImageApi._internal();

  Future<Images> findByMovieId(int id) async {
    checkConnectivity(throwIfNot: true);
    var url = '${env.Api.baseUrl}/movie/$id/images?api_key=${env.Api.key}';
    print('get ~> $url');
    http.Response response = await http.get(url);
    return utils.Decode.image(response);
  }
}
