import 'dart:async';

import 'package:flutter_movie/models/movie.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class MovieRepository {
  static final MovieRepository _singleton = MovieRepository._internal();

  factory MovieRepository() => _singleton;

  MovieRepository._internal();

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await _init();
    return _db;
  }

  _init() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'movie.db');
    print('db $path');

    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  FutureOr<void> _onCreate(Database db, int version) async {
    await db.execute(
      'create table movie('
      'id integer primary key, '
      'title text, '
      'overview text, '
      'release_date text, '
      'original_title text, '
      'poster_path text, '
      'backdrop_path text, '
      'vote_count integer, '
      'popularity real, '
      'vote_average real'
      ')',
    );
  }

  Future<List<Movie>> findAll() async {
    final client = await db;
    final sql = 'select * from movie order by vote_average desc';
    print(sql);
    final results = await client.rawQuery(sql);
    if (results.isNotEmpty) {
      return results.map((json) => Movie.fromJson(json)).toList();
    }
    return [];
  }

  Future<int> save(Movie movie) async {
    final client = await db;
    final sql = 'insert or replace into movie('
        'id, '
        'title, '
        'overview, '
        'release_date, '
        'original_title, '
        'poster_path, '
        'backdrop_path, '
        'vote_count, '
        'popularity, '
        'vote_average'
        ') values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
    print(sql);
    final id = await client.rawInsert(sql, [
      movie.id,
      movie.title,
      movie.overview,
      movie.releaseDate,
      movie.originalTitle,
      movie.posterPath,
      movie.backdropPath,
      movie.voteCount,
      movie.popularity,
      movie.voteAverage
    ]);
    return id;
  }

  Future<bool> exists(int id) async {
    final client = await db;
    final sql = 'select id from movie where id = ?';
    print(sql);
    final results = await client.rawQuery(sql, [id]);
    return results.isNotEmpty;
  }

  Future<int> delete(int id) async {
    final client = await db;
    final sql = 'delete from movie where id = ?';
    print(sql);
    return await client.rawDelete(sql, [id]);
  }

  Future<void> close() async {
    final client = await db;
    return client.close();
  }
}
