import 'package:flutter/material.dart';
import 'package:flutter_movie/environment.dart' as env;
import 'package:flutter_movie/models/movie.dart';
import 'package:flutter_movie/utils.dart' as utils;
import 'package:flutter_rating/flutter_rating.dart';
import 'package:transparent_image/transparent_image.dart';

class MovieTile extends StatefulWidget {
  final Movie movie;

  const MovieTile(this.movie);

  @override
  _MovieTileState createState() => _MovieTileState();
}

class _MovieTileState extends State<MovieTile> {
  Movie get movie => widget.movie;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      padding: EdgeInsets.all(8),
      color: Colors.black,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: _poster(movie),
          ),
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.only(top: 2, left: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    movie.title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      color: Colors.white,
                      decoration: TextDecoration.combine([]),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        StarRating(
                          size: 16,
                          starCount: 10,
                          rating: movie.voteAverage,
                          color: Colors.red,
                          borderColor: Colors.white,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: Text(
                      movie.overview,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.white,
                        decoration: TextDecoration.combine([]),
                      ),
                      maxLines: 5,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _poster(movie) {
    Widget child;
    if (movie.posterPath == null) {
      child = utils.Widget.noimage();
    } else {
      var url = '${env.Api.imageUrl}/w500${movie.posterPath}';
      child = FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: url,
        fit: BoxFit.cover,
      );
    }
    return AspectRatio(
      aspectRatio: 1200 / 1800,
      child: child,
    );
  }
}
