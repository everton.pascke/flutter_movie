import 'package:json_annotation/json_annotation.dart';
import 'movie.dart';

part 'pagination.g.dart';

@JsonSerializable()
class Pagination {

  @JsonKey(name: 'page') int page;
  @JsonKey(name: 'total_pages') int totalPages;
  @JsonKey(name: 'total_results') int totalResults;
  @JsonKey(name: 'results') List<Movie> results;

  Pagination({
    this.page,
    this.totalPages,
    this.totalResults
  });

  factory Pagination.fromJson(Map<String, dynamic> json) => _$PaginationFromJson(json);
  Map<String, dynamic> toJson() => _$PaginationToJson(this);
}