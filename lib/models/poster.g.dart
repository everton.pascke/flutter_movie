// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'poster.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Poster _$PosterFromJson(Map<String, dynamic> json) {
  return Poster(
      width: json['width'] as int,
      height: json['height'] as int,
      voteCount: json['vote_count'] as int,
      filePath: json['file_path'] as String,
      aspectRatio: (json['aspect_ratio'] as num)?.toDouble(),
      voteAverage: (json['vote_average'] as num)?.toDouble());
}

Map<String, dynamic> _$PosterToJson(Poster instance) => <String, dynamic>{
      'width': instance.width,
      'height': instance.height,
      'vote_count': instance.voteCount,
      'file_path': instance.filePath,
      'vote_average': instance.voteAverage,
      'aspect_ratio': instance.aspectRatio
    };
