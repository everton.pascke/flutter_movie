// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'companie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Companie _$CompanieFromJson(Map<String, dynamic> json) {
  return Companie(
      id: json['id'] as int,
      name: json['name'] as String,
      logoPath: json['logo_path'] as String,
      originCountry: json['origin_country'] as String);
}

Map<String, dynamic> _$CompanieToJson(Companie instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'logo_path': instance.logoPath,
      'origin_country': instance.originCountry
    };
