// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Images _$ImagesFromJson(Map<String, dynamic> json) {
  return Images(
      id: json['id'] as int,
      posters: (json['posters'] as List)
          ?.map((e) =>
              e == null ? null : Poster.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      backdrops: (json['backdrops'] as List)
          ?.map((e) =>
              e == null ? null : Backdrop.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$ImagesToJson(Images instance) => <String, dynamic>{
      'id': instance.id,
      'posters': instance.posters,
      'backdrops': instance.backdrops
    };
