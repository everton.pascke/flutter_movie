import 'dart:convert';
import 'package:http/http.dart' as http;

import 'images.dart';
import 'movie.dart';
import 'pagination.dart';

class Decode {

  static Images image(http.Response response) {
    var statusCode = response.statusCode;
    if (statusCode == 200) {
      var decoded = json.decode(response.body);
      return Images.fromJson(decoded);
    } else {
      handleException(response);
    }
  }

  static Movie movie(http.Response response) {
    var statusCode = response.statusCode;
    if (statusCode == 200) {
      var decoded = json.decode(response.body);
      return Movie.fromJson(decoded);
    } else {
      handleException(response);
    }
  }

  static Pagination pagination(http.Response response) {
    var statusCode = response.statusCode;
    if (statusCode == 200) {
      var decoded = json.decode(response.body);
      return Pagination.fromJson(decoded);
    } else {
      handleException(response);
    }
  }

  static void handleException(http.Response response) {
    var statusCode = response.statusCode;
    if (statusCode == 401 || statusCode == 404) {
      var decoded = json.decode(response.body);
      throw Exception(decoded['status_message']);
    } else {
      throw Exception('Falha ao obter dados do servidor.');
    }
  }
}