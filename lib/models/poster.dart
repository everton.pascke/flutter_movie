import 'package:json_annotation/json_annotation.dart';

part 'poster.g.dart';

@JsonSerializable()
class Poster {

  @JsonKey(name: 'width') int width;
  @JsonKey(name: 'height') int height;
  @JsonKey(name: 'vote_count') int voteCount;

  @JsonKey(name: 'file_path') String filePath;

  @JsonKey(name: 'vote_average') double voteAverage;
  @JsonKey(name: 'aspect_ratio') double aspectRatio;

  Poster({
    this.width,
    this.height,
    this.voteCount,
    this.filePath,
    this.aspectRatio,
    this.voteAverage
  });

  factory Poster.fromJson(Map<String, dynamic> json) => _$PosterFromJson(json);
  Map<String, dynamic> toJson() => _$PosterToJson(this);
}