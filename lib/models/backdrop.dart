import 'package:json_annotation/json_annotation.dart';

part 'backdrop.g.dart';

@JsonSerializable()
class Backdrop {

  @JsonKey(name: 'width') int width;
  @JsonKey(name: 'height') int height;
  @JsonKey(name: 'vote_count') int voteCount;

  @JsonKey(name: 'file_path') String filePath;

  @JsonKey(name: 'vote_average') double voteAverage;
  @JsonKey(name: 'aspect_ratio') double aspectRatio;

  Backdrop({
    this.width,
    this.height,
    this.voteCount,
    this.filePath,
    this.aspectRatio,
    this.voteAverage
  });

  factory Backdrop.fromJson(Map<String, dynamic> json) => _$BackdropFromJson(json);
  Map<String, dynamic> toJson() => _$BackdropToJson(this);
}