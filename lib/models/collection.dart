import 'package:json_annotation/json_annotation.dart';

part 'collection.g.dart';

@JsonSerializable()
class Collection {

  @JsonKey(name: 'id') int id;
  @JsonKey(name: 'name') String name;
  @JsonKey(name: 'poster_path') String posterPath;
  @JsonKey(name: 'backdrop_path') String backdropPath;

  Collection({this.id, this.name, this.posterPath, this.backdropPath});

  factory Collection.fromJson(Map<String, dynamic> json) => _$CollectionFromJson(json);
  Map<String, dynamic> toJson() => _$CollectionToJson(this);
}