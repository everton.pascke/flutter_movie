class Error {

  int statusCode;
  String statusMessage;

  Error({this.statusCode, this.statusMessage});

  factory Error.fromJson(Map<String, dynamic> json){
    return Error(
      statusCode: json['status_code'],
      statusMessage: json['status_message']
    );
  }
}