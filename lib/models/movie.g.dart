// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Movie _$MovieFromJson(Map<String, dynamic> json) {
  return Movie(
      id: json['id'] as int,
      budget: json['budget'] as int,
      revenue: json['revenue'] as int,
      runtime: json['runtime'] as int,
      voteCount: json['vote_count'] as int,
      title: json['title'] as String,
      status: json['status'] as String,
      imdbId: json['imdb_id'] as String,
      tagline: json['tagline'] as String,
      homepage: json['homepage'] as String,
      overview: json['overview'] as String,
      posterPath: json['poster_path'] as String,
      releaseDate: json['release_date'] as String,
      backdropPath: json['backdrop_path'] as String,
      originalTitle: json['original_title'] as String,
      originalLanguage: json['original_language'] as String,
      video: json['video'] as bool,
      adult: json['adult'] as bool,
      popularity: (json['popularity'] as num)?.toDouble(),
      voteAverage: (json['vote_average'] as num)?.toDouble(),
      collection: json['belongs_to_collection'] == null
          ? null
          : Collection.fromJson(
              json['belongs_to_collection'] as Map<String, dynamic>),
      genreIds: (json['genre_ids'] as List)?.map((e) => e as int)?.toList(),
      genres: (json['genres'] as List)
          ?.map((e) =>
              e == null ? null : Genre.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      companies: (json['production_companies'] as List)
          ?.map((e) =>
              e == null ? null : Companie.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$MovieToJson(Movie instance) => <String, dynamic>{
      'id': instance.id,
      'budget': instance.budget,
      'revenue': instance.revenue,
      'runtime': instance.runtime,
      'vote_count': instance.voteCount,
      'title': instance.title,
      'status': instance.status,
      'imdb_id': instance.imdbId,
      'tagline': instance.tagline,
      'homepage': instance.homepage,
      'overview': instance.overview,
      'poster_path': instance.posterPath,
      'release_date': instance.releaseDate,
      'backdrop_path': instance.backdropPath,
      'original_title': instance.originalTitle,
      'original_language': instance.originalLanguage,
      'video': instance.video,
      'adult': instance.adult,
      'popularity': instance.popularity,
      'vote_average': instance.voteAverage,
      'belongs_to_collection': instance.collection,
      'genre_ids': instance.genreIds,
      'genres': instance.genres,
      'production_companies': instance.companies
    };
