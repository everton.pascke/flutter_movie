import 'package:json_annotation/json_annotation.dart';

import 'genre.dart';
import 'poster.dart';
import 'companie.dart';
import 'backdrop.dart';
import 'collection.dart';

part 'movie.g.dart';

@JsonSerializable()
class Movie {

  @JsonKey(name: 'id') int id;
  @JsonKey(name: 'budget') int budget;
  @JsonKey(name: 'revenue') int revenue;
  @JsonKey(name: 'runtime') int runtime;
  @JsonKey(name: 'vote_count') int voteCount;

  @JsonKey(name: 'title') String title;
  @JsonKey(name: 'status') String status;
  @JsonKey(name: 'imdb_id') String imdbId;
  @JsonKey(name: 'tagline') String tagline;
  @JsonKey(name: 'homepage') String homepage;
  @JsonKey(name: 'overview') String overview;
  @JsonKey(name: 'poster_path') String posterPath;
  @JsonKey(name: 'release_date') String releaseDate;
  @JsonKey(name: 'backdrop_path') String backdropPath;
  @JsonKey(name: 'original_title') String originalTitle;
  @JsonKey(name: 'original_language') String originalLanguage;

  @JsonKey(name: 'video') bool video;
  @JsonKey(name: 'adult') bool adult;

  @JsonKey(name: 'popularity') double popularity;
  @JsonKey(name: 'vote_average') double voteAverage;

  @JsonKey(name: 'belongs_to_collection') Collection collection;

  @JsonKey(name: 'genre_ids') List<int> genreIds;
  @JsonKey(name: 'genres') List<Genre> genres;
  @JsonKey(name: 'production_companies') List<Companie> companies;

  Movie({
    this.id,
    this.budget,
    this.revenue,
    this.runtime,
    this.voteCount,
    this.title,
    this.status,
    this.imdbId,
    this.tagline,
    this.homepage,
    this.overview,
    this.posterPath,
    this.releaseDate,
    this.backdropPath,
    this.originalTitle,
    this.originalLanguage,
    this.video,
    this.adult,
    this.popularity,
    this.voteAverage,
    this.collection,
    this.genreIds,
    this.genres,
    this.companies,
  });

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);
  Map<String, dynamic> toJson() => _$MovieToJson(this);
}