import 'package:json_annotation/json_annotation.dart';

import 'poster.dart';
import 'backdrop.dart';

part 'images.g.dart';

@JsonSerializable()
class Images {

  @JsonKey(name: 'id') int id;
  @JsonKey(name: 'posters') List<Poster> posters;
  @JsonKey(name: 'backdrops') List<Backdrop> backdrops;

  Images({this.id, this.posters, this.backdrops});

  factory Images.fromJson(Map<String, dynamic> json) => _$ImagesFromJson(json);
  Map<String, dynamic> toJson() => _$ImagesToJson(this);
}