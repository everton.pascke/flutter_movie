// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'backdrop.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Backdrop _$BackdropFromJson(Map<String, dynamic> json) {
  return Backdrop(
      width: json['width'] as int,
      height: json['height'] as int,
      voteCount: json['vote_count'] as int,
      filePath: json['file_path'] as String,
      aspectRatio: (json['aspect_ratio'] as num)?.toDouble(),
      voteAverage: (json['vote_average'] as num)?.toDouble());
}

Map<String, dynamic> _$BackdropToJson(Backdrop instance) => <String, dynamic>{
      'width': instance.width,
      'height': instance.height,
      'vote_count': instance.voteCount,
      'file_path': instance.filePath,
      'vote_average': instance.voteAverage,
      'aspect_ratio': instance.aspectRatio
    };
