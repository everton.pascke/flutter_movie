import 'package:json_annotation/json_annotation.dart';

part 'companie.g.dart';

@JsonSerializable()
class Companie {

  @JsonKey(name: 'id') int id;
  @JsonKey(name: 'name') String name;
  @JsonKey(name: 'logo_path') String logoPath;
  @JsonKey(name: 'origin_country') String originCountry;

  Companie({this.id, this.name, this.logoPath, this.originCountry});

  factory Companie.fromJson(Map<String, dynamic> json) => _$CompanieFromJson(json);
  Map<String, dynamic> toJson() => _$CompanieToJson(this);
}