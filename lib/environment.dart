class Api {

  static final String key = '9410af425ba592e4330aa4a18752ceda';
  static final String baseUrl = 'https://api.themoviedb.org/3';
  static final String imageUrl = 'https://image.tmdb.org/t/p';
  static final String language = 'pt-BR';
}