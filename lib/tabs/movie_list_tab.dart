import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/blocs/movie_popular_bloc.dart';
import 'package:flutter_movie/environment.dart' as env;
import 'package:flutter_movie/models/movie.dart';
import 'package:flutter_movie/models/pagination.dart';
import 'package:flutter_movie/pages/movie_detail_page.dart';
import 'package:flutter_movie/utils.dart' as utils;
import 'package:rxdart/rxdart.dart';
import 'package:transparent_image/transparent_image.dart';

class MovieListTab extends StatefulWidget {
  @override
  _MovieListTabState createState() => _MovieListTabState();
}

class _MovieListTabState extends State<MovieListTab>
    with AutomaticKeepAliveClientMixin<MovieListTab> {
  ScrollController _controller;
  Pagination _pagination;
  MoviePopularBloc _bloc = BlocProvider.getBloc<MoviePopularBloc>();

  StreamController _loaderController = PublishSubject<bool>();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController()..addListener(_onScrollListener);
    _bloc.findAllPopulars(1);
  }

  @override
  void dispose() {
    _loaderController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder<Pagination>(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        Widget child;
        if (snapshot.hasError) {
          child = _error(snapshot.error);
        } else if (snapshot.hasData) {
          _config(snapshot.data);
          child = _stack();
        }
        if (child != null) {
          return RefreshIndicator(
            child: child,
            color: Colors.red,
            onRefresh: _onRefresh,
          );
        }
        return utils.Widget.progress();
      },
    );
  }

  _config(Pagination pagination) {
    List<Movie> results;
    if (_pagination != null && _pagination.page != pagination.page) {
      results = _pagination.results;
    }
    _pagination = pagination;
    if (results != null) {
      _pagination.results.insertAll(0, results);
    }
  }

  _onScrollListener() {
    if (_controller.position.pixels == _controller.position.maxScrollExtent) {
      if (_pagination != null) {
        _next();
      }
    }
  }

  _next() {
    if (_pagination.page < _pagination.totalPages) {
      _loaderController.sink.add(true);
      _bloc.findAllPopulars(_pagination.page + 1);
    }
  }

  _error(Object error) {
    _loaderController.sink.add(false);
    return Center(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: 250,
            child: utils.Widget.error(
              error,
              color: Colors.red,
            ),
          ),
        ],
      ),
    );
  }

  _stack() {
    _loaderController.sink.add(false);
    return Stack(
      children: <Widget>[
        _grid(),
        _loader(),
      ],
    );
  }

  _grid() {
    return GridView(
      controller: _controller,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        childAspectRatio: 16 / 9,
      ),
      children: _pagination.results.map((movie) {
        Widget child;
        if (movie.backdropPath == null) {
          child = utils.Widget.noimage();
        } else {
          var src = '${env.Api.imageUrl}/w500${movie.backdropPath}';
          child = FadeInImage.memoryNetwork(
            placeholder: kTransparentImage,
            image: src,
            fit: BoxFit.cover,
          );
        }
        return InkWell(
          onTap: () => _onMovieTap(movie),
          child: child,
        );
      }).toList(),
    );
  }

  _onMovieTap(movie) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => MovieDetailPage(movie)));
  }

  _loader() {
    return StreamBuilder<bool>(
      stream: _loaderController.stream,
      initialData: false,
      builder: (context, snapshot) {
        if (snapshot.data) {
          return Align(
            child: Container(
              width: 70.0,
              height: 70.0,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 24),
                child: utils.Widget.progress(),
              ),
            ),
            alignment: FractionalOffset.bottomCenter,
          );
        }
        return new SizedBox(
          width: 0,
          height: 0,
        );
      },
    );
  }

  Future<void> _onRefresh() {
    _pagination = null;
    return _bloc.findAllPopulars(1);
  }
}
