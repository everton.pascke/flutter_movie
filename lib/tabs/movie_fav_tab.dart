import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/blocs/movie_repository_bloc.dart';
import 'package:flutter_movie/models/movie.dart';
import 'package:flutter_movie/pages/movie_detail_page.dart';
import 'package:flutter_movie/tiles/movie_tile.dart';
import 'package:flutter_movie/utils.dart' as utils;

class MovieFavTab extends StatefulWidget {
  @override
  _MovieFavTabState createState() => _MovieFavTabState();
}

class _MovieFavTabState extends State<MovieFavTab> {
  MovieRepositoryBloc _movieRepositoryBloc =
      BlocProvider.getBloc<MovieRepositoryBloc>();

  @override
  void initState() {
    super.initState();
    _movieRepositoryBloc.findAll();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Movie>>(
      stream: _movieRepositoryBloc.findAllStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _list(snapshot.data);
        }
        return utils.Widget.progress();
      },
    );
  }

  _list(List<Movie> movies) {
    return ListView.builder(
      itemCount: movies.length,
      itemBuilder: (context, index) {
        Movie movie = movies[index];
        return _item(movie);
      },
    );
  }

  _item(Movie movie) {
    return InkWell(
      onTap: () => _onMovieTap(movie),
      child: MovieTile(movie),
    );
  }

  _onMovieTap(movie) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => MovieDetailPage(movie)));
  }
}
