import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/blocs/movie_repository_bloc.dart';
import 'package:flutter_movie/pages/home_page.dart';

import 'blocs/image_bloc.dart';
import 'blocs/movie_popular_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => MoviePopularBloc()),
        Bloc((i) => ImageBloc()),
        Bloc((i) => MovieRepositoryBloc()),
      ],
      child: MaterialApp(
        title: 'Flutter Movie',
        theme: ThemeData.dark(),
        debugShowCheckedModeBanner: false,
        home: HomePage(),
      ),
    );
  }
}