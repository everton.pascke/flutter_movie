import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/blocs/image_bloc.dart';
import 'package:flutter_movie/blocs/movie_repository_bloc.dart';
import 'package:flutter_movie/environment.dart' as env;
import 'package:flutter_movie/models/backdrop.dart';
import 'package:flutter_movie/models/images.dart';
import 'package:flutter_movie/models/movie.dart';
import 'package:flutter_movie/tiles/movie_tile.dart';
import 'package:flutter_movie/utils.dart' as utils;
import 'package:shimmer/shimmer.dart';
import 'package:transparent_image/transparent_image.dart';

class MovieDetailPage extends StatefulWidget {
  final Movie movie;

  MovieDetailPage(this.movie);

  @override
  _MovieDetailPageState createState() => _MovieDetailPageState();
}

class _MovieDetailPageState extends State<MovieDetailPage> {
  Movie get movie => widget.movie;

  bool _exists = false;
  Images _images;
  ImageBloc _imageBloc = BlocProvider.getBloc<ImageBloc>();
  MovieRepositoryBloc _movieRepositoryBloc =
      BlocProvider.getBloc<MovieRepositoryBloc>();

  @override
  void initState() {
    super.initState();
    int id = movie.id;
    // LOAD
    _imageBloc.findByMovieId(id);
    _movieRepositoryBloc.exists(id);
    // LISTEN
    _movieRepositoryBloc.saveStream
        .listen((_) => _movieRepositoryBloc.exists(id));
    _movieRepositoryBloc.deleteStream
        .listen((_) => _movieRepositoryBloc.exists(id));
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _onRefresh,
      color: Colors.red,
      child: CustomScrollView(
        primary: true,
        slivers: <Widget>[
          _bar(),
          _body(),
        ],
      ),
    );
  }

  _bar() {
    return SliverAppBar(
      title: Text(
        movie.title,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      floating: false,
      actions: <Widget>[
        IconButton(
          icon: StreamBuilder<bool>(
            stream: _movieRepositoryBloc.existsStream,
            initialData: false,
            builder: (context, snapshot) {
              _exists = snapshot.data;
              if (_exists) {
                return Icon(
                  Icons.favorite,
                  color: Colors.red,
                );
              }
              return Icon(
                Icons.favorite,
                color: Colors.grey[500],
              );
            },
          ),
          onPressed: _onFavPressed,
        )
      ],
      expandedHeight: 250,
      flexibleSpace: FlexibleSpaceBar(
        background: _header(),
      ),
    );
  }

  _body() {
    return SliverList(
      delegate: SliverChildListDelegate([
        _detail(),
        _carousel(),
      ]),
    );
  }

  _header() {
    Widget child;
    if (movie.backdropPath == null) {
      child = utils.Widget.noimage();
    } else {
      var url = '${env.Api.imageUrl}/w500${movie.backdropPath}';
      child = FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: url,
        fit: BoxFit.cover,
      );
    }
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: child,
    );
  }

  _detail() {
    return MovieTile(movie);
  }

  _carousel() {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.black,
          padding: EdgeInsets.only(top: 8, left: 8, bottom: 8),
          alignment: Alignment.centerLeft,
          child: Text(
            'Imagens de fundo',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.normal,
              decoration: TextDecoration.combine([]),
            ),
          ),
        ),
        StreamBuilder<Images>(
          stream: _imageBloc.stream,
          builder: (context, snapshot) {
            List<Widget> children;
            if (_images != null) {
              children = _backdrops();
            } else {
              if (snapshot.hasError) {
                children = _error(snapshot.error);
              } else if (snapshot.hasData) {
                _images = snapshot.data;
                children = _backdrops();
              } else {
                children = List<Widget>.generate(3, (_) {
                  return Shimmer.fromColors(
                    baseColor: Colors.white,
                    highlightColor: Colors.grey,
                    child: Container(
                      color: Colors.white,
                    ),
                  );
                });
              }
            }
            return Container(
              color: Colors.black,
              child: CarouselSlider(
                autoPlay: false,
                aspectRatio: 2.0,
                enlargeCenterPage: true,
                enableInfiniteScroll: true,
                items: children.map((Widget child) {
                  return Container(
                    margin: EdgeInsets.all(2),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      child: child,
                    ),
                  );
                }).toList(),
              ),
            );
          },
        ),
      ],
    );
  }

  _backdrops() {
    if (_images.backdrops.isEmpty) {
      return List<Widget>.generate(3, (_) => utils.Widget.noimage());
    }
    return _images.backdrops.map((Backdrop backdrop) {
      var url = '${env.Api.imageUrl}/w500${backdrop.filePath}';
      return FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: url,
        fit: BoxFit.cover,
      );
    }).toList();
  }

  _onFavPressed() async {
    if (_exists) {
      _movieRepositoryBloc.delete(movie.id);
    } else {
      _movieRepositoryBloc.save(movie);
    }
  }

  _error(Object error) {
    return List<Widget>.generate(3, (_) {
      return Container(
        color: Colors.white,
        child: utils.Widget.error(
          error,
          color: Colors.grey[500],
        ),
      );
    });
  }

  Future<void> _onRefresh() {
    return _imageBloc.findByMovieId(movie.id);
  }
}
