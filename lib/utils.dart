import 'dart:io';

import 'package:flutter/material.dart';

class Label {

  static String INTERNET_NOT_AVAILABLE = 'Internet indisponível.\nVerifique a sua conexão!';
}

class Widget {

  static noimage() {
    return Image.asset(
      'assets/images/placeholder.jpg',
      fit: BoxFit.cover,
    );
  }

  static progress() {
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
      ),
    );
  }

  static error(Object error, {color = Colors.red, fontSize = 18.0}) {
    String message;
    if (error is SocketException) {
      message = Label.INTERNET_NOT_AVAILABLE;
    } else {
      message = error.toString().replaceAll('Exception: ', '');
    }
    return Center(
      child: Text(
        message,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontWeight: FontWeight.bold,
          decoration: TextDecoration.combine([]),
        ),
      ),
    );
  }
}

