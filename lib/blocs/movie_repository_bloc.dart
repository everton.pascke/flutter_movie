import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_movie/apis/movie_api.dart';
import 'package:flutter_movie/db/movie_repository.dart';
import 'package:flutter_movie/models/movie.dart';
import 'package:rxdart/rxdart.dart';

class MovieRepositoryBloc extends BlocBase {

  MovieRepository _repository = MovieRepository();

  StreamController _saveController = PublishSubject<int>();
  StreamController _deleteController = PublishSubject<int>();
  StreamController _existsController = PublishSubject<bool>();
  StreamController _findAllController = PublishSubject<List<Movie>>();

  Stream<int> get saveStream => _saveController.stream;
  Stream<int> get deleteStream => _deleteController.stream;
  Stream<bool> get existsStream => _existsController.stream;
  Stream<List<Movie>> get findAllStream => _findAllController.stream;

  void exists(int id) async {
    bool val = await _repository.exists(id);
    _existsController.sink.add(val);
  }

  void save(Movie movie) async {
    int val = await _repository.save(movie);
    _saveController.sink.add(val);
    findAll(); // update
  }

  void delete(int id) async {
    int val = await _repository.delete(id);
    _deleteController.sink.add(val);
    findAll(); // update
  }

  void findAll() async {
    List<Movie> val = await _repository.findAll();
    _findAllController.sink.add(val);
  }

  @override
  void dispose() {
    _repository.close();
    _saveController.close();
    _deleteController.close();
    _existsController.close();
    _findAllController.close();
    super.dispose();
  }
}
