import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_movie/apis/image_api.dart';
import 'package:flutter_movie/models/images.dart';
import 'package:rxdart/rxdart.dart';

class ImageBloc extends BlocBase {

  ImageApi _api = ImageApi();
  
  StreamController _controller = PublishSubject<Images>();

  Stream<Images> get stream => _controller.stream;

  Future<void> findByMovieId(int id) async {
    Future<Images> future = _api.findByMovieId(id);
    future.then((images) {
      _controller.sink.add(images);
    }).catchError((error) {
      _controller.sink.addError(error);
    });
    return future;
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}