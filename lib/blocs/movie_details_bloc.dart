import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_movie/apis/movie_api.dart';
import 'package:flutter_movie/models/movie.dart';
import 'package:rxdart/rxdart.dart';

class MoviePopularBloc extends BlocBase {
  MovieApi _api = MovieApi();

  StreamController _controller = PublishSubject<Movie>();

  Stream<Movie> get stream => _controller.stream;

  void findById(int id) async {
    try {
      Movie movie = await _api.findById(id);
      _controller.sink.add(movie);
    } on Exception catch (error) {
      _controller.sink.addError(error);
    }
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}
