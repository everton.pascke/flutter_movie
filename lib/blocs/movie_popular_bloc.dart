import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_movie/apis/movie_api.dart';
import 'package:flutter_movie/models/pagination.dart';
import 'package:rxdart/rxdart.dart';

class MoviePopularBloc extends BlocBase {
  MovieApi _api = MovieApi();

  StreamController _controller = PublishSubject<Pagination>();

  Stream<Pagination> get stream => _controller.stream;

  Future<Pagination> findAllPopulars(int page) {
    Future<Pagination> future = _api.findAllPopulars(page);
    future.then((Pagination pagination) {
      _controller.sink.add(pagination);
    }).catchError((error) {
      _controller.sink.addError(error);
    });
    return future;
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}
