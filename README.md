# flutter_movie

[The Movie Database](https://www.themoviedb.org/) Flutter application.

## Screens

<table width="100%" cellspacing="4" border="0">
    <tr>
        <td>![alt text](../prints/print_1.png)</td>
        <td>![alt text](../prints/print_2.png)</td>
    </tr>
    <tr>
        <td>![alt text](../prints/print_3.png)</td>
        <td>![alt text](../prints/print_4.png)</td>
    </tr>
    <tr>
        <td>![alt text](../prints/print_5.png)</td>
        <td></td>
    </tr>
</table>
